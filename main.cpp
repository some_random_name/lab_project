#include <iostream>
#include <stdlib.h>     //for system("cls");
#include <fstream>      //File support
#include <string>       //String support
#include <sstream>      //string, int conversion
#include <time.h>       //measuring time
#include <windows.h>
#include <cmath>        //round function
#include <regex>        //regex

using namespace std;

class FileStream
{
public:
    string time_tab[4];
    int time_tab_i[4];
    fstream file;
    string data;
    string temp;

    void load()
    {
        regex wzorzec( "length: (\\d{1,2})" );
        int i=0;
        data.clear();
        file.open( "Prog_Data.txt", ios::in );
        if( file.good() == true )
        {
            while(getline(file, temp))
            {
                data = data + "\n" + temp;
                //regex for finding time
                smatch wynik;
                if( regex_search( temp, wynik, wzorzec ) )
                {
                    time_tab[i] = wynik.str(1);
                    i++;
                }
            }
            for (i=0;i<4;i++)
            {
                istringstream ss(time_tab[i]);
                ss >> time_tab_i[i];
            }
        }
        else cout << "I couldn't load programs data :(" << endl;
        file.close();
    }

    void read_prog(int prog_num)
    {
        string prog_num_str;
        string end_str;
        int prog_num_1 = prog_num +1;
        stringstream ss;
        ss << prog_num;
        prog_num_str = ss.str();        //number of program (string)
        stringstream ss2;
        ss2 << prog_num_1;
        end_str = ss2.str();             //number of next program (ending, string)

        data.clear();
        fstream file;
        string temp;
        file.open( "Prog_Data.txt", ios::in );
        while(getline(file, temp))
        {
            if(temp==prog_num_str)
            {
                while(getline(file, temp) && temp!=end_str)
                {
                    data = data + "\n" + temp;
                }
            }
        }
    }
};

class Interface : public FileStream
{
public:
    int prog_num;
    char decision;
    void display_start()
    {
        system("cls");
        load();
        cout << "This is the Virtual Machine!" << endl;
        cout << "You can select from the following programs:" << endl;
        cout << data << endl;
        cout << "---------------------------------------------" << endl;
        cout << "You can select the program by typing it's number or press (Q) to quit" << endl;
        while(true)
        {
            Sleep(70);
            if(GetKeyState('Q') & 0x8000)  {exit(0);}
            else if(GetKeyState('1') & 0x8000)
                {prog_num = 1;break;}
            else if(GetKeyState('2') & 0x8000)
                {prog_num = 2;break;}
            else if(GetKeyState('3') & 0x8000)
                {prog_num = 3;break;}
            else if(GetKeyState('4') & 0x8000)
                {prog_num = 4;break;}
            else continue;
        }
    }

    void prog_running()
    {
        clock_t start, end, pause_start, pause_end;
        double time_used, pause_time = 0;
        int time_of_prog = time_tab_i[prog_num-1];

        start = clock();
        while(time_used < time_of_prog) //time_tab_i[prog_num-1]
        {
            pause_start = clock();
            if(GetKeyState('P') & 0x8000/*Check if high-order bit is set (1 << 15)*/)
                {cout<<"Program stopped, press any key to continue\n";system("pause");}
            if(GetKeyState('S') & 0x8000)
                {break;}
            pause_end = clock();
            pause_time += (double)(pause_end - pause_start)/ CLOCKS_PER_SEC;
            end = clock();
            time_used = (double)(end - start)/ CLOCKS_PER_SEC - pause_time;

            system("cls");
            cout << "Program number "<< prog_num <<" is running"<< endl;
            cout<<"Time: "<<round(time_used)<<"sec/"<<time_of_prog<<"sec"<<"\n";
            cout << "To pause the program press (P) and to stop the program press (S)"<< endl;
            Sleep(50);
        }
        cout << "Program ended!" <<endl;
        system("pause");
    }

    void display_prog()
    {
        read_prog(prog_num);
        system("cls");
        cout << "You selected program number "<< prog_num << endl;
        cout << data <<endl;
        cout << "Do you want to start (Y/N)"<< endl<<endl;
        cin >> decision;
        if (decision=='y' || decision=='Y') Interface::prog_running();
    }
};

int main()
{
    Interface i1;
    while(true)
    {
        i1.display_start();
        i1.display_prog();
    }

    return 0;
}
